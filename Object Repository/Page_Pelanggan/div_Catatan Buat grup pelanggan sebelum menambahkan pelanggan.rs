<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Catatan Buat grup pelanggan sebelum menambahkan pelanggan</name>
   <tag></tag>
   <elementGuidId>87e372ae-db52-446c-8823-796118eae67a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class = 'col-sm-12']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.col-sm-12</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div/div[2]/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-sm-12</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
				                    
                        Catatan: Buat grup pelanggan sebelum menambahkan pelanggan.                    
                                
                                                                                                
					
                                                                        
                                Telepon *
                            
                            
                                
                            
                            
                                The Telepon field is required.                            
                        
                    					
                                            
                            Nama *
                        
                        
                            
                        
                        
                            The Nama field is required.                        
                    
					
					                        
                            Jenis Pelanggan *
                        
                        
                            
Individu
Corporate

                        
                        
                                                    
                    
					
					                        
                            PIC *
                        
                        
                            
                        
                        
                            The PIC field is required.                        
                    
					
					                        
                            Jenis Kelamin *
                        
                        
                            
Pria
Wanita

                        
                        
                                                    
                    
					
                                            
                            E-mail *                        
                        
                            
                        
                        
                            The E-mail field is required.                        
                    

                    					
					                        
                            Alamat *
                        
                        
                            
                        
                        
                            The Alamat field is required.                        
                    
					
					                        
                            NPWP                        
                        
                            
                        
                        
                                                    
                    
					
                    					                        
                            Nomor Registrasi                        
                        
                            
                        
                        
                                                    
                    
                    					
					                        
                            Tanggal PKS                        
                        
                            
                        
                        
                                                    
                    
					
					                        
                            Tanggal Berakhir PKS                        
                        
                            
                        
                        
                                                    
                    
					
					
                                        
                        
                            Grup *
                        
                        
                                          No matches found
Grup Keling
Grup Bendungan Hilir
Kost Bendungan Walahar
Grup Auto 1
Grup Auto 2
Grup Auto 3
Grup Auto 4

                        
                        
                            The Grup field is required.                        
                    
                                        
                                            
                            Foto 
                        
                        
                            
                                
                                
                                    
                                        
                                        Bersih                                    
                                    
                                        
                                        
                                        Browse File
                                        
                                    
                                
                            
                            * Maksimal ukuran photo 1 Mb
                        

                        
                                                    
                    

                    
                        
                            Keterangan                        
                        
                            
                        
                        
                                                    
                    

                    
                        
                            
                        
                    
                
            </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;skin-blue fuelux  pace-done&quot;]/div[@class=&quot;wrapper row-offcanvas row-offcanvas-left&quot;]/aside[@class=&quot;right-side&quot;]/section[@class=&quot;content&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12&quot;]/div[@class=&quot;box&quot;]/div[@class=&quot;box-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-sm-12&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tambah Pelanggan'])[1]/following::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pelanggan'])[4]/following::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[2]/div/div</value>
   </webElementXpaths>
</WebElementEntity>
