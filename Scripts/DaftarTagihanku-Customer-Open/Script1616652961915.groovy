import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Test Cases/Login'), [('Username') : '11223344555', ('Password') : 'P@ssw0rd'], FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/DaftarTagihanku-Customer-Open/Page_Beranda/a_Daftar Tagihanku'))

WebUI.click(findTestObject('Object Repository/DaftarTagihanku-Customer-Open/Page_List Tagihanku/icon_Edit'))

WebUI.click(findTestObject('Object Repository/DaftarTagihanku-Customer-Open/Page_List Tagihanku/input_Detail_select_data_radio'))

WebUI.scrollToElement(findTestObject('Object Repository/DaftarTagihanku-Customer-Open/Page_List Tagihanku/a_Pilih Metode Pembayaran'), 2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/DaftarTagihanku-Customer-Open/Page_List Tagihanku/a_Pilih Metode Pembayaran'))

WebUI.click(findTestObject('Object Repository/DaftarTagihanku-Customer-Open/Page_List Tagihanku/a_Bayar dengan BRIVA'))

WebUI.acceptAlert(FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/DaftarTagihanku-Customer-Open/Rating/button_LAIN KALI'))

WebUI.delay(7)

WebUI.click(findTestObject('Object Repository/DaftarTagihanku-Customer-Open/Page_List Tagihanku/span_user-account'))

WebUI.click(findTestObject('Object Repository/DaftarTagihanku-Customer-Open/Page_List Tagihanku/a_Keluar'))

WebUI.closeBrowser()
