import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('https://beta-panel.brismartbilling.id/')

WebUI.maximizeWindow()

WebUI.setText(findTestObject('Object Repository/Sign In/Page_Sign in/input_Username'), Username)

WebUI.setText(findTestObject('Object Repository/Sign In/Page_Sign in/input_Password'), Password)

WebUI.click(findTestObject('Object Repository/Sign In/Page_Sign in/submit_Signin'))

WebUI.delay(2)

if (WebUI.verifyElementPresent(findTestObject('Object Repository/Sign In/Page_Sign in/Page_Beranda/img'), 7, FailureHandling.OPTIONAL)) {
    WebUI.comment('Login Sukses')
} else if (WebUI.verifyElementPresent(findTestObject('Object Repository/Sign In/Validasi Sign In/Incorrect Signin'), 3, 
    FailureHandling.OPTIONAL)) {
    WebUI.comment('Incorrect Signin')
} else if (WebUI.verifyElementPresent(findTestObject('Object Repository/Sign In/Validasi Sign In/The Username field is required'), 
    3, FailureHandling.OPTIONAL)) {
    WebUI.comment('The Username field is required')
} else if (WebUI.verifyElementPresent(findTestObject('Object Repository/Sign In/Validasi Sign In/The Password field is required'), 
    3, FailureHandling.OPTIONAL)) {
    WebUI.comment('The Password field is required')
}

