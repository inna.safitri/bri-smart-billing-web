import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Test Cases/Login'), [('Username') : 'administrator', ('Password') : 'Admin123'], FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/TambahPerusahaan/Page_Beranda/a_Perusahaan'))

WebUI.setText(findTestObject('Object Repository/Update/UpdatePaymentPerusahaan/input_Search'), inputSearch)

WebUI.click(findTestObject('Object Repository/Update/UpdatePaymentPerusahaan/icon-edit'))

WebUI.scrollToElement(findTestObject('Object Repository/Update/UpdatePaymentPerusahaan/label_Tipe Pembayaran'), 2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Update/UpdatePaymentPerusahaan/pilih_TipePembayaran'))

WebUI.setText(findTestObject('Object Repository/Update/UpdatePaymentPerusahaan/input_TipePembayaran'), TipePembayaran)

WebUI.sendKeys(findTestObject('Object Repository/Update/UpdatePaymentPerusahaan/input_TipePembayaran'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Update/UpdatePaymentPerusahaan/btn btn-success'))

WebUI.delay(5)
