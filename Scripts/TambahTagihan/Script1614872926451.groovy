import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.Keys as Keys

//WebUI.callTestCase(findTestCase('Test Cases/Login'), [('Username') : 'admin_apartemen', ('Password') : 'Admin123!'], FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/TambahTagihan/Page_Tagihan/Pilih_Grup'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/TambahTagihan/Page_Tagihan/input_Grup'), Grup)

WebUI.sendKeys(findTestObject('Object Repository/TambahTagihan/Page_Tagihan/input_Grup'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/TambahTagihan/Page_Tagihan/Pilih_ID_Pelanggan'))

WebUI.setText(findTestObject('Object Repository/TambahTagihan/Page_Tagihan/input_ID_Pelanggan'), ID_Pelanggan)

WebUI.sendKeys(findTestObject('Object Repository/TambahTagihan/Page_Tagihan/input_ID_Pelanggan'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/TambahTagihan/Page_Tagihan/Pilih_Jenis_Tagihan'))

WebUI.setText(findTestObject('Object Repository/TambahTagihan/Page_Tagihan/input_Jenis_Tagihan'), JenisTagihan)

WebUI.sendKeys(findTestObject('Object Repository/TambahTagihan/Page_Tagihan/input_Jenis_Tagihan'), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('Object Repository/TambahTagihan/Page_Tagihan/input_Amount_Tagihan'), Amount)

WebUI.setText(findTestObject('Object Repository/TambahTagihan/Page_Tagihan/input_Diskon_discount'), Diskon)

WebUI.setText(findTestObject('Object Repository/TambahTagihan/Page_Tagihan/input_Catatan_remarks'), Catatan)

WebUI.setText(findTestObject('Object Repository/TambahTagihan/Page_Tagihan/input_Jatuh_Tempo'), JatuhTempo)

WebUI.sendKeys(findTestObject('Object Repository/TambahTagihan/Page_Tagihan/input_Jatuh_Tempo'), Keys.chord(Keys.TAB))

WebUI.click(findTestObject('Object Repository/TambahTagihan/Page_Tagihan/Submit_Tagihan'))

WebUI.delay(2)

if (WebUI.verifyElementPresent(findTestObject('Object Repository/Sign In/Page_Sign in/Page_Beranda/img'), 5, FailureHandling.OPTIONAL))
{
	WebUI.comment('Sukses Tambah Tagihan')
}
else if (WebUI.verifyElementPresent(findTestObject('Object Repository/TambahTagihan/ValidasiTambahTagihan/p_The Grup field is required'), 5, FailureHandling.OPTIONAL))
{
	WebUI.comment('Grup harus diisi')
}
else if (WebUI.verifyElementPresent(findTestObject('Object Repository/TambahTagihan/ValidasiTambahTagihan/p_The Jenis Tagihan field is required'), 5, FailureHandling.OPTIONAL))
{
	WebUI.comment('Jenis Tagihan harus diisi')
}
else if (WebUI.verifyElementPresent(findTestObject('Object Repository/TambahTagihan/ValidasiTambahTagihan/p_The Tagihan field is required'), 5, FailureHandling.OPTIONAL))
{
	WebUI.comment('Tagihan harus diisi')
}
else if (WebUI.verifyElementPresent(findTestObject('Object Repository/TambahTagihan/ValidasiTambahTagihan/p_The Diskon field must contain only numbers'), 5, FailureHandling.OPTIONAL))
{
	WebUI.comment('Diskon harus diisi')
}
else if (WebUI.verifyElementPresent(findTestObject('Object Repository/TambahTagihan/ValidasiTambahTagihan/p_Tanggal is not valid dd-mm-yyyy'), 5, FailureHandling.OPTIONAL))
{
	WebUI.comment('Tanggal tidak valid')
}

WebUI.delay(3)
