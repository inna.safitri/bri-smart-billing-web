import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.Keys as Keys

//WebUI.callTestCase(findTestCase('Test Cases/Login'), [('Username') : 'admin_kosjaya', ('Password') : 'Nyamnyam123'], FailureHandling.CONTINUE_ON_FAILURE)
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/TambahPelanggan/Page_Pelanggan/a_Tambah Pelanggan'))

WebUI.setText(findTestObject('Object Repository/TambahPelanggan/Page_Pelanggan/input_Telepon'), Telepon)

WebUI.sendKeys(findTestObject('Object Repository/TambahPelanggan/Page_Pelanggan/input_Telepon'), Keys.chord(Keys.TAB))

WebUI.setText(findTestObject('Object Repository/TambahPelanggan/Page_Pelanggan/input_Nama'), NamaPelanggan)

//WebUI.selectOptionByValue(findTestObject('Object Repository/TambahPelanggan/Page_Pelanggan/pilih_IndividuCorporate'), JenisPelanggan, false)
//WebUI.verifyOptionSelectedByValue(findTestObject('Object Repository/TambahPelanggan/Page_Pelanggan/pilih_IndividuCorporate'), JenisPelanggan, false, 2)
WebUI.setText(findTestObject('Object Repository/TambahPelanggan/Page_Pelanggan/input_PIC'), PIC)

//WebUI.selectOptionByValue(findTestObject('Object Repository/TambahPelanggan/Page_Pelanggan/pilih_PriaWanita'), JenisKelamin, false)
//WebUI.verifyOptionSelectedByValue(findTestObject('Object Repository/TambahPelanggan/Page_Pelanggan/pilih_PriaWanita'), JenisKelamin, false, 2)
WebUI.setText(findTestObject('Object Repository/TambahPelanggan/Page_Pelanggan/input_Email'), Email)

WebUI.sendKeys(findTestObject('Object Repository/TambahPelanggan/Page_Pelanggan/input_Email'), Keys.chord(Keys.TAB))

WebUI.delay(20)

WebUI.sendKeys(findTestObject('Object Repository/TambahPelanggan/Page_Pelanggan/input_Alamat'), Keys.chord(Keys.CONTROL,'a'))

WebUI.sendKeys(findTestObject('Object Repository/TambahPelanggan/Page_Pelanggan/input_Alamat'), Keys.chord(Keys.BACK_SPACE))

WebUI.setText(findTestObject('Object Repository/TambahPelanggan/Page_Pelanggan/input_Alamat'), Alamat, FailureHandling.OPTIONAL)

WebUI.scrollToElement(findTestObject('Object Repository/TambahPelanggan/Page_Pelanggan/label_NPWP'), 2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/TambahPelanggan/Page_Pelanggan/input_NPWP'), NPWP)

WebUI.setText(findTestObject('Object Repository/TambahPelanggan/Page_Pelanggan/input_Nomor Registrasi'), NoRegistrasi)

WebUI.setText(findTestObject('Object Repository/TambahPelanggan/Page_Pelanggan/input_Tanggal PKS'), TanggalPKS)

WebUI.sendKeys(findTestObject('Object Repository/TambahPelanggan/Page_Pelanggan/input_Tanggal PKS'), Keys.chord(Keys.TAB))

WebUI.setText(findTestObject('Object Repository/TambahPelanggan/Page_Pelanggan/input_Tanggal Berakhir PKS'), TanggalAkhirPKS)

WebUI.sendKeys(findTestObject('Object Repository/TambahPelanggan/Page_Pelanggan/input_Tanggal Berakhir PKS'), Keys.chord(Keys.TAB))

WebUI.setText(findTestObject('Object Repository/TambahPelanggan/Page_Pelanggan/input_GrupPelanggan'), GrupPelanggan)

WebUI.sendKeys(findTestObject('Object Repository/TambahPelanggan/Page_Pelanggan/input_GrupPelanggan'), Keys.chord(Keys.ENTER))

WebUI.delay(3)

WebUI.sendKeys(findTestObject('Object Repository/TambahPelanggan/Page_Pelanggan/input_GrupPelanggan'), Keys.chord(Keys.TAB))

WebUI.uploadFile(findTestObject('Object Repository/TambahPelanggan/Page_Pelanggan/input_BrowseFileFoto'), Foto, FailureHandling.OPTIONAL)

WebUI.setText(findTestObject('Object Repository/TambahPelanggan/Page_Pelanggan/input_Keterangan'), Keterangan)

WebUI.delay(5)

//WebUI.click(findTestObject('null'))

//WebUI.click(findTestObject('null'))

//WebUI.scrollToElement(findTestObject('Object Repository/TambahPelanggan/Page_Pelanggan/btn-success'), 1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/TambahPelanggan/Page_Pelanggan/submit_pelanggan'))

WebUI.delay(2)

if (WebUI.verifyElementPresent(findTestObject('Object Repository/TambahPelanggan/Page_Pelanggan/div_ID Pelanggan'), 30, 
    FailureHandling.OPTIONAL)) {
    WebUI.comment('Sukses Tambah Pelanggan')
} else if (WebUI.verifyElementPresent(findTestObject('Object Repository/TambahPelanggan/ValidasiTambahPelanggan/p_The Telepon field is required'), 
    30, FailureHandling.OPTIONAL)) {
    WebUI.comment('Telepon wajib diisi')
} else if (WebUI.verifyElementPresent(findTestObject('Object Repository/TambahPelanggan/ValidasiTambahPelanggan/p_The Telepon field must be at least 10 characters in length'), 
    30, FailureHandling.OPTIONAL)) {
    WebUI.comment('Telepon tidak boleh kurang dari 10 digit')
} else if (WebUI.verifyElementPresent(findTestObject('Object Repository/TambahPelanggan/ValidasiTambahPelanggan/p_The Nama field is required'), 
    30, FailureHandling.OPTIONAL)) {
    WebUI.comment('Nama wajib diisi')
} else if (WebUI.verifyElementPresent(findTestObject('Object Repository/TambahPelanggan/ValidasiTambahPelanggan/p_The PIC field is required'), 
    30, FailureHandling.OPTIONAL)) {
    WebUI.comment('PIC wajib diisi')
} else if (WebUI.verifyElementPresent(findTestObject('Object Repository/TambahPelanggan/ValidasiTambahPelanggan/p_The E-mail field is required'), 
    30, FailureHandling.OPTIONAL)) {
    WebUI.comment('Email wajib diisi')
} else if (WebUI.verifyElementPresent(findTestObject('Object Repository/TambahPelanggan/ValidasiTambahPelanggan/p_The Alamat field is required'), 
    30, FailureHandling.OPTIONAL)) {
    WebUI.comment('Alamat wajib diisi')
} else if (WebUI.verifyElementPresent(findTestObject('Object Repository/TambahPelanggan/ValidasiTambahPelanggan/p_The Grup field is required'), 
    30, FailureHandling.OPTIONAL)) {
    WebUI.comment('Grup wajib diisi')
} else if (WebUI.verifyElementPresent(findTestObject('Object Repository/TambahPelanggan/ValidasiTambahPelanggan/div_-- Customer exist,email harus sama dengan customer exist --'), 
    30, FailureHandling.OPTIONAL)) {
    WebUI.comment('Customer Exist')
}

WebUI.delay(2)
