import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.google.common.collect.FilteredEntryMultimap.Keys as Keys
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.Keys as Keys

//WebUI.callTestCase(findTestCase('Test Cases/Login'), [('Username') : 'administrator', ('Password') : 'Admin123'], FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/TambahPerusahaan/Page_Perusahaan/a_Tambah Perusahaan'))

WebUI.setText(findTestObject('Object Repository/TambahPerusahaan/Page_Perusahaan/input_NamaPerusahaan'), NamaPerusahaan)

WebUI.check(findTestObject('Object Repository/TambahPerusahaan/Page_Perusahaan/pilih_SegmenProduk'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/TambahPerusahaan/Page_Perusahaan/pilih_Segmen'))

WebUI.setText(findTestObject('Object Repository/TambahPerusahaan/Page_Perusahaan/input_Segmen'), Segmen)

WebUI.sendKeys(findTestObject('Object Repository/TambahPerusahaan/Page_Perusahaan/input_Segmen'), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('Object Repository/TambahPerusahaan/Page_Perusahaan/input_TanggalBergabung'), TanggalBergabung)

WebUI.sendKeys(findTestObject('Object Repository/TambahPerusahaan/Page_Perusahaan/input_TanggalBergabung'), Keys.chord(Keys.TAB))

WebUI.setText(findTestObject('Object Repository/TambahPerusahaan/Page_Perusahaan/input_Email'), Email)

WebUI.setText(findTestObject('Object Repository/TambahPerusahaan/Page_Perusahaan/input_Telepon'), Telepon)

WebUI.setText(findTestObject('Object Repository/TambahPerusahaan/Page_Perusahaan/input_NoRekening'), NoRekening)

WebUI.setText(findTestObject('Object Repository/TambahPerusahaan/Page_Perusahaan/input_Alamat'), Alamat)

WebUI.scrollToElement(findTestObject('Object Repository/TambahPerusahaan/Page_Perusahaan/label_Provinsi'), 1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/TambahPerusahaan/Page_Perusahaan/pilih_Provinsi'))

WebUI.setText(findTestObject('Object Repository/TambahPerusahaan/Page_Perusahaan/input_Provinsi'), Provinsi)

WebUI.sendKeys(findTestObject('Object Repository/TambahPerusahaan/Page_Perusahaan/input_Provinsi'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/TambahPerusahaan/Page_Perusahaan/pilih_TipePembayaran'))

WebUI.setText(findTestObject('Object Repository/TambahPerusahaan/Page_Perusahaan/input_TipePembayaran'), TipePembayaran)

WebUI.sendKeys(findTestObject('Object Repository/TambahPerusahaan/Page_Perusahaan/input_TipePembayaran'), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('Object Repository/TambahPerusahaan/Page_Perusahaan/input_CompanyCode'), CompanyCode)

WebUI.selectOptionByLabel(findTestObject('Object Repository/TambahPerusahaan/Page_Perusahaan/select_JenisPelanggan'), JenisPelanggan, false)

WebUI.verifyOptionSelectedByLabel(findTestObject('Object Repository/TambahPerusahaan/Page_Perusahaan/select_JenisPelanggan'), JenisPelanggan, false, 1)

WebUI.click(findTestObject('Object Repository/TambahPerusahaan/Page_Perusahaan/input__btn btn-success'))

WebUI.delay(2)

if (WebUI.verifyElementPresent(findTestObject('Object Repository/Sign In/Page_Sign in/Page_Beranda/img'), 10, FailureHandling.OPTIONAL))
{
	WebUI.comment('Sukses Tambah Perusahaan')
}
else if (WebUI.verifyElementPresent(findTestObject('Object Repository/TambahPerusahaan/ValidasiTambahPelanggan/p_The Nama field is required'), 5, FailureHandling.OPTIONAL))
{
	WebUI.comment('Nama Perusahaan harus diisi')
}
else if (WebUI.verifyElementPresent(findTestObject('Object Repository/TambahPerusahaan/ValidasiTambahPelanggan/p_The Segmen field is required'), 5, FailureHandling.OPTIONAL))
{
	WebUI.comment('Segmen harus dipilih')
}
else if (WebUI.verifyElementPresent(findTestObject('Object Repository/TambahPerusahaan/ValidasiTambahPelanggan/p_The  field is required'), 5, FailureHandling.OPTIONAL))
{
	WebUI.comment('Tanggal Bergabung harus diisi')
}
else if (WebUI.verifyElementPresent(findTestObject('Object Repository/TambahPerusahaan/ValidasiTambahPelanggan/p_The Email field is required'), 5, FailureHandling.OPTIONAL))
{
	WebUI.comment('Email harus diisi')
}
else if (WebUI.verifyElementPresent(findTestObject('Object Repository/TambahPerusahaan/ValidasiTambahPelanggan/p_Email already exists'), 5, FailureHandling.OPTIONAL))
{
	WebUI.comment('Email sudah pernah digunakan')
}
else if (WebUI.verifyElementPresent(findTestObject('Object Repository/TambahPerusahaan/ValidasiTambahPelanggan/p_The Email field must contain a valid email address'), 5, FailureHandling.OPTIONAL))
{
	WebUI.comment('Email tidak valid')
}
else if (WebUI.verifyElementPresent(findTestObject('Object Repository/TambahPerusahaan/ValidasiTambahPelanggan/p_The Telepon field is required'), 5, FailureHandling.OPTIONAL))
{
	WebUI.comment('Telepon harus diisi')
}
else if (WebUI.verifyElementPresent(findTestObject('Object Repository/TambahPerusahaan/ValidasiTambahPelanggan/p_The Telepon field must contain only numbers'), 5, FailureHandling.OPTIONAL))
{
	WebUI.comment('Telepon harus berupa angka')
}
else if (WebUI.verifyElementPresent(findTestObject('Object Repository/TambahPerusahaan/ValidasiTambahPelanggan/p_The Telepon field must be at least 5 characters in length'), 5, FailureHandling.OPTIONAL))
{
	WebUI.comment('Minimal input Telepon 5 karakter ')
}
else if (WebUI.verifyElementPresent(findTestObject('Object Repository/TambahPerusahaan/ValidasiTambahPelanggan/p_Rekening not found'), 5, FailureHandling.OPTIONAL))
{
	WebUI.comment('Rekening harus diisi')
}
else if (WebUI.verifyElementPresent(findTestObject('Object Repository/TambahPerusahaan/ValidasiTambahPelanggan/p_The Alamat field is required'), 5, FailureHandling.OPTIONAL))
{
	WebUI.comment('Alamat harus diisi')
}
else if (WebUI.verifyElementPresent(findTestObject('Object Repository/TambahPerusahaan/ValidasiTambahPelanggan/p_The Provinsi field is required'), 5, FailureHandling.OPTIONAL))
{
	WebUI.comment('Provinsi harus dipilih')
}
else if (WebUI.verifyElementPresent(findTestObject('Object Repository/TambahPerusahaan/ValidasiTambahPelanggan/p_The Tipe Pembayaran field is required'), 5, FailureHandling.OPTIONAL))
{
	WebUI.comment('Tipe Pembayaran harus dipilih')
}
else if (WebUI.verifyElementPresent(findTestObject('Object Repository/TambahPerusahaan/ValidasiTambahPelanggan/p_The Kode Perusahaan field is required'), 5, FailureHandling.OPTIONAL))
{
	WebUI.comment('Kode Perusahaan harus diisi')
}
else if (WebUI.verifyElementPresent(findTestObject('Object Repository/TambahPerusahaan/ValidasiTambahPelanggan/p_Kode Perusahaan not found'), 5, FailureHandling.OPTIONAL))
{
	WebUI.comment('Kode Perusahaan tidak ditemukan')
}

WebUI.delay(3)
