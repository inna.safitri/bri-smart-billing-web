import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.Keys as Keys

//WebUI.callTestCase(findTestCase('Test Cases/Login'), [('Username') : 'admin_apartemen', ('Password') : 'Admin123!'], FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/TambahGrupPelanggan/Page_Grup Pelanggan/a_Tambah Grup Pelanggan'))

WebUI.setText(findTestObject('Object Repository/TambahGrupPelanggan/Page_Grup Pelanggan/input_Grup'), GrupPelanggan)

WebUI.click(findTestObject('Object Repository/TambahGrupPelanggan/Page_Grup Pelanggan/input__btn btn-success'))

WebUI.delay(2)

if (WebUI.verifyElementPresent(findTestObject('Object Repository/Sign In/Page_Sign in/Page_Beranda/img'), 5, FailureHandling.OPTIONAL)) 
{
	WebUI.comment('Sukses Tambah Grup Pelanggan')
} 
else if (WebUI.verifyElementPresent(findTestObject('Object Repository/TambahGrupPelanggan/ValidasiGrupPelanggan/p_The Grup field is required'), 5, FailureHandling.OPTIONAL))
{
	WebUI.comment('Grup wajib diisi')
}
else if (WebUI.verifyElementPresent(findTestObject('Object Repository/TambahGrupPelanggan/ValidasiGrupPelanggan/p_Grup already exists'), 5, FailureHandling.OPTIONAL))
{
	WebUI.comment('Grup already exists')
}

WebUI.delay(3)

