import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.Keys as Keys

//WebUI.callTestCase(findTestCase('Test Cases/Login'), [('Username') : 'admin_apartemen', ('Password') : 'Admin123!'], FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/TambahPengguna/Page_Pengguna/a_Tambah Pengguna'))

WebUI.setText(findTestObject('Object Repository/TambahPengguna/Page_Pengguna/input_NamaPengguna'), Nama)

WebUI.selectOptionByLabel(findTestObject('Object Repository/TambahPengguna/Page_Pengguna/select_Pilih Jenis KelaminPriaWanita'), JenisKelamin, false)

WebUI.verifyOptionSelectedByLabel(findTestObject('Object Repository/TambahPengguna/Page_Pengguna/select_Pilih Jenis KelaminPriaWanita'), JenisKelamin, false, 2)

WebUI.setText(findTestObject('Object Repository/TambahPengguna/Page_Pengguna/input_Email'), Email)

WebUI.setText(findTestObject('Object Repository/TambahPengguna/Page_Pengguna/input_Telepon'), Telepon)

WebUI.setText(findTestObject('Object Repository/TambahPengguna/Page_Pengguna/input_Alamat'), Alamat)

//WebUI.uploadFile(findTestObject('Object Repository/TambahPengguna/Page_Pengguna/input_Pilih File Foto'), FileFoto, FailureHandling.OPTIONAL)
//WebUI.click(BrowseFile)
//WebUI.click(PilihFileFoto)
//WebUI.click(OK)

WebUI.selectOptionByLabel(findTestObject('Object Repository/TambahPengguna/Page_Pengguna/pilih_Peran'), Peran, false)

WebUI.verifyOptionSelectedByLabel(findTestObject('Object Repository/TambahPengguna/Page_Pengguna/pilih_Peran'), Peran, false, 2)

WebUI.scrollToElement(findTestObject('Object Repository/TambahPengguna/Page_Pengguna/label_Grup Pelanggan'), 2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/TambahPengguna/Page_Pengguna/span_Pilih Grup Pelanggan'))

WebUI.setText(findTestObject('Object Repository/TambahPengguna/Page_Pengguna/input_Grup Pelanggan'), GrupPelanggan)

WebUI.sendKeys(findTestObject('Object Repository/TambahPengguna/Page_Pengguna/input_Grup Pelanggan'), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('Object Repository/TambahPengguna/Page_Pengguna/input_Username'), NamaPengguna)

//WebUI.setText(findTestObject('Object Repository/TambahPengguna/Page_Pengguna/input_Password'), KataSandi)

WebUI.click(findTestObject('Object Repository/TambahPengguna/Page_Pengguna/input__btn btn-success'))

WebUI.delay(2)

if (WebUI.verifyElementPresent(findTestObject('Object Repository/Sign In/Page_Sign in/Page_Beranda/img'), 10, FailureHandling.OPTIONAL)) 
{
	WebUI.comment('Sukses Tambah Pengguna')
} 
else if (WebUI.verifyElementPresent(findTestObject('Object Repository/TambahPengguna/ValidasiPengguna/Page_Pengguna/p_The Nama field is required'), 10, FailureHandling.OPTIONAL)) 
{
	WebUI.comment('Nama Pengguna diisi')
} 
else if (WebUI.verifyElementPresent(findTestObject('Object Repository/TambahPengguna/ValidasiPengguna/Page_Pengguna/p_The E-mail field is required'), 10, FailureHandling.OPTIONAL)) 
{
	WebUI.comment('Email harus diisi')
} 
else if (WebUI.verifyElementPresent(findTestObject('Object Repository/TambahPengguna/ValidasiPengguna/Page_Pengguna/p_The Peran field is required'), 10, FailureHandling.OPTIONAL)) 
{
	WebUI.comment('Peran harus diisi')
} 
else if (WebUI.verifyElementPresent(findTestObject('Object Repository/TambahPelanggan/ValidasiTambahPelanggan/p_The Grup field is required'), 10, FailureHandling.OPTIONAL))
{
	WebUI.comment('Grup Pelanggan harus diisi')
}
else if (WebUI.verifyElementPresent(findTestObject('Object Repository/TambahPengguna/ValidasiPengguna/Page_Pengguna/p_Nama Pengguna already exists'), 10, FailureHandling.OPTIONAL)) 
{
	WebUI.comment('Username sudah pernah digunakan')
} 
else if (WebUI.verifyElementPresent(findTestObject('Object Repository/TambahPengguna/ValidasiPengguna/Page_Pengguna/p_Kata Sandi must include at least one number, one letter, and one CAPS'), 10, FailureHandling.OPTIONAL)) 
{
	WebUI.comment('Password harus diisi')
} 

WebUI.delay(3)
